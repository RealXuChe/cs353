import {ToolMeta} from "@/inlcude/tool_metadata";

const meta: ToolMeta = {
    name: "CIDR Calculator",
    route: "/toolbox/cidr-calculator",
}

export default meta;